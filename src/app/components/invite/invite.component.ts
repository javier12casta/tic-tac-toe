import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { MultiplayerService } from 'src/app/services/multiplayer.service';


@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.scss']
})
export class InviteComponent implements OnInit {

  
  shareURL: string;
  inviteMessage: string;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private multiPlayerService: MultiplayerService) { }

  async ngOnInit() {
    this.inviteMessage = 'Inviata a un amigo:  ';
    this.multiPlayerService.gameId$.pipe().subscribe(gameid => {
      if (gameid) {
        this.setShareURL(gameid);
        console.log(this.shareURL);
      }
    })
  }
  setShareURL(gameid: string) {
    this.shareURL = gameid;
  }

}
