import { Component, OnInit } from '@angular/core';
import { GameDTO } from 'src/app/models/game';
import { MultiplayerService } from 'src/app/services/multiplayer.service';
import { ViewEncapsulation } from '@angular/core';
import { faHandPointUp } from '@fortawesome/free-solid-svg-icons';
import {NgForm} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuComponent implements OnInit {
   faHandPointUp = faHandPointUp;
   gamedto = new GameDTO;
   code: string = null

  constructor(private multiPlayerService: MultiplayerService,
    private router: Router,) { }

  ngOnInit() {
    this.multiPlayerService.gameId$.subscribe(id => {
      this.gamedto.id = id;
    });
    
  }

  joinGame(){
    let str : string = (<HTMLInputElement>document.getElementById("codeJoin")).value;
    console.log(str);
    this.code = str;
    this.router.navigate(['/game',this.code])
  }



}
