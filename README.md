# Tictactoe

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.

see app [Tictactoe app](https://tictactoe-84d2b.web.app/)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Deploy firebase
Run `ng build` to build the project.

Install `npm install -g firebase-tools`.

Run `firebase login` ang login, It will ask if you want your information about using the tool to be shared with Firebase or not,
type Y to accept or N for no, then hit enter.

Run `firebase init`. Using the up / down arrows select which Firebase feature you will use, once you have selected the Hosting feature, press the space bar to activate the use of this feature, then press Enter

Using the up / down arrows select the project with which you will integrate the current folder

write the project directory for example
What do you want to use as your public directory? (public) dist / tictactoe

finally run `firebase deploy` to deploy your application

## FUTURE CHANGES

for future changes the following commands must be executed

`ng build`
`firebase deploy`


